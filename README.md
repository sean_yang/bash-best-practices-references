# Bash, Bash Best Practices and References

## Links to Bash Best Practices References

- Templates to write better Bash scripts http://bash3boilerplate.sh
- BashPitfalls https://mywiki.wooledge.org/BashPitfalls#echo_.3C.3CEOF
- Beginner Mistakes https://wiki.bash-hackers.org/scripting/newbie_traps
- The Bash Hackers Wiki [Bash Hackers Wiki] https://wiki.bash-hackers.org/
- Cheat sheets https://bertvv.github.io/cheat-sheets/
- Good practices for writing shell scripts http://www.yoone.eu/articles/2-good-practices-for-writing-shell-scripts.html
- Best Practices for Writing Bash Scripts https://kvz.io/blog/2013/11/21/bash-best-practices/
- Shell Scripting - Best Practices https://fahdshariff.blogspot.com/2013/10/shell-scripting-best-practices.html
- What you need to know about bash functions http://blog.joncairns.com/2013/08/what-you-need-to-know-about-bash-functions/
- Licenses https://choosealicense.com/licenses/
- 
## Other references
- Bash tips: Colors and formatting (ANSI/VT100 Control sequences) https://misc.flogisoft.com/bash/tip_colors_and_formatting
- Bert Van Vreckem's https://github.com/bertvv/dotfiles
- Advanced Bash-Scripting Guide http://www.tldp.org/LDP/abs/html/
- Small code snippets https://wiki.bash-hackers.org/snipplets/start
- Single responsibility principle https://en.wikipedia.org/wiki/Single_responsibility_principle
- Scripting: If Comparison Operators In Bash https://blog.100tb.com/scripting-if-comparison-operators-in-bash
- Linux File System: Chapter 3. The Root Filesystem http://refspecs.linuxfoundation.org/FHS_3.0/fhs/ch03.html
- Prompt for sudo password and programmatically elevate privilege in bash script? https://unix.stackexchange.com/questions/28791/prompt-for-sudo-password-and-programmatically-elevate-privilege-in-bash-script
- /usr/bin vs /usr/local/bin on Linux https://unix.stackexchange.com/questions/8656/usr-bin-vs-usr-local-bin-on-linux
- What color codes can I use in my PS1 prompt? https://unix.stackexchange.com/questions/124407/what-color-codes-can-i-use-in-my-ps1-prompt
- Bash Conditional Expressions https://www.gnu.org/software/bash/manual/html_node/Bash-Conditional-Expressions.html
- Argbash documentation https://argbash.readthedocs.io/en/stable/index.html
- Semantic Versioning 2.0.0 https://semver.org/